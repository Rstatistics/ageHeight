I'm trying out the tidy data paradigm on a small set of data. 
There are 3 samples of n = 100.

Each has 4 columns: name, age, random, height.
---
steps:

2: run the file "tidyTheData.R" to import the .csv files

1: (optional)
    delete or rename the .csv files and recreate them by running "3datasets.R"
    
---
Workflow is described by Hadley Wickham 
https://www.youtube.com/watch?v=rz3_FDVt9eg
---
|   | idea | package   |
|---|:-:|--:|
|1.|nested data| tidyr|
|2.|functional programming|purrr|
|3.|Models -> tidy data | broom|

