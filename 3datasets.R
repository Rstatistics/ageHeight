# this R script makes 3 random sets of age~height data and saves them to .csv files
# another script will re-import these, group according to tidy data principles and graph
#
#
# generate first 3 columns
rand1= runif(100, min= .8, max = 1.2)
rand2= runif(100, min=.95, max = 1.4)
rand3= runif(100, min= .4, max = 1.0)
n = 1:100
y1 = runif(100, min = 20, max = 80)
y2 = runif(100, min = 12, max = 37)
y3 = runif(100, min = 35, max = 55)

# combine the first 3 columns of each dataframe
df1 <- data.frame(n,y1,rand1)
df2 <- data.frame(n,y2,rand2)
df3 <- data.frame(n,y3,rand3)
# generate the fourth column (height ~ f(age,rand))
df1[4] <- 23 + 6*y1*rand1
df2[4] <- 18 + 6*y2*rand2
df3[4] <- 23 + 6*y3*rand3
# name the columns
colnames(df1) <-c("name","age", "rand(.8-1.2)", "height")
colnames(df2) <-c("name","age", "rand(.95-1.4)", "height")
colnames(df3) <-c("name","age", "rand(.4-1)", "height")
#export the data frames to separate csv files
write.csv(df1,file="df1.csv", row.names= FALSE)
write.csv(df2,file="df2.csv", row.names= FALSE)
write.csv(df1,file="df3.csv", row.names= FALSE)
